﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.Auth;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper) {

            _postHub = postHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        public async Task<CommentDTO> UpdateComment(CommentUpdateDTO commentDto)
        {
        var originalComment = await _context.Comments.FirstAsync(comment => comment.Id == commentDto.CommentId);
            originalComment.Body = commentDto.Body;
            originalComment.UpdatedAt = System.DateTime.Now;
            _context.Comments.Update(originalComment);
            await _context.SaveChangesAsync();

            var updatedComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(comment => comment.Id == commentDto.CommentId);

            var updatedCommentDTO = _mapper.Map<CommentDTO>(updatedComment);
            await _postHub.Clients.All.SendAsync("UpdateComment", updatedCommentDTO);

            return updatedCommentDTO;
        }
        public async Task RemoveComment(int id)
        {
            var comment = _context.Comments.Find(id);
            if (comment != null)
            {
                comment.IsDeleted = true;
                _context.Comments.Update(comment);
                await _context.SaveChangesAsync();
                await _postHub.Clients.All.SendAsync("DeleteComment", _mapper.Map<CommentDTO>(comment));
            }

        }
    }
}
