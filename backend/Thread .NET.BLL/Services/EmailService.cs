﻿using AutoMapper;
using System;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace Thread_.NET.BLL.Services
{
    public sealed class EmailService 
    {

        private readonly IConfiguration Configuration;
        public EmailService(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task SendMail(String message, string email)
        {
            MailAddress from = new MailAddress(Configuration["Email"], "Tom");
            MailAddress to = new MailAddress(email);
            MailMessage m = new MailMessage(from, to);
            m.Subject = "Notification";
            m.Body = message;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new NetworkCredential(Configuration["Email"], Configuration["Password"]);
            smtp.EnableSsl = true;
            await smtp.SendMailAsync(m);
            Console.WriteLine("Письмо отправлено");

        }
    }
}
