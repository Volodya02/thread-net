﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly EmailService _emailService;
        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, EmailService emailService) : base(context, mapper)
        {
            _emailService = emailService;
            _postHub = postHub;
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.FirstOrDefault(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if(likes==null)
            {
                _context.PostReactions.Add(new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
                    
                await _context.SaveChangesAsync();
                var authorid = _context.Posts.Find(reaction.EntityId).AuthorId;
                await _postHub.Clients.User(authorid.ToString()).SendAsync("SomeoneLikedYourPost", _context.Users.Find(reaction.UserId).UserName);
                await _emailService.SendMail($"{ _context.Users.Find(reaction.UserId).UserName} liked yours post",_context.Users.Find(authorid).Email);
                return;
            }
            if(likes.IsLike==reaction.IsLike)
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            likes.IsLike = reaction.IsLike;
            likes.UpdatedAt = System.DateTime.Now;
            _context.PostReactions.Update(likes);

            await _context.SaveChangesAsync();
        }
        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.FirstOrDefault(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes == null)
            {
                _context.CommentReactions.Add(new DAL.Entities.CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });

                await _context.SaveChangesAsync();
                return;
            }
            if (likes.IsLike == reaction.IsLike)
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            likes.IsLike = reaction.IsLike;
            likes.UpdatedAt = System.DateTime.Now;
            _context.CommentReactions.Update(likes);

            await _context.SaveChangesAsync();

        }
    }
}
