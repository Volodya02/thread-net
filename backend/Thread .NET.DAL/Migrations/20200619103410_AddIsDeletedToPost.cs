﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddIsDeletedToPost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 18, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3301), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3305), 9 },
                    { 2, 5, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2768), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2783), 8 },
                    { 3, 6, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2818), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2823), 10 },
                    { 4, 7, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2847), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2852), 16 },
                    { 5, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2876), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2881), 12 },
                    { 6, 8, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2902), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2906), 12 },
                    { 7, 17, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2927), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2931), 9 },
                    { 8, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2953), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2957), 5 },
                    { 9, 18, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3026), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3031), 11 },
                    { 10, 11, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3052), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3056), 16 },
                    { 1, 15, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(1638), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2316), 21 },
                    { 12, 7, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3102), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3106), 2 },
                    { 13, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3127), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3132), 6 },
                    { 14, 16, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3153), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3157), 18 },
                    { 15, 18, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3178), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3182), 17 },
                    { 16, 20, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3202), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3207), 12 },
                    { 17, 17, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3227), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3231), 18 },
                    { 18, 16, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3252), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3257), 20 },
                    { 19, 16, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3277), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3281), 9 },
                    { 11, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3077), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3082), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Veniam velit nisi qui maiores iste qui et.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(2398), 20, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3066) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Consequatur nesciunt ipsum est qui qui sunt dolores eos quisquam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3629), 14, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3644) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Assumenda qui cumque quaerat aut nihil aut quo aut.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3774), 15, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3780) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quo autem consectetur quis est illo non.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3844), 8, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3848) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Odit dolorem incidunt aspernatur commodi quam aut tempore itaque sed.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3922), 11, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3927) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Ea at quia praesentium qui.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3990), 1, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3995) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Eius laudantium numquam corporis ducimus vel.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4056), 9, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4060) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Modi ut quam consequatur deleniti debitis accusantium officiis.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4179), 14, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4184) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Quia consequatur ipsum repellat corporis alias temporibus iusto.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4247), 12, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4252) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Ut laboriosam voluptatum rerum commodi sit autem repudiandae eaque ut.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4322), 6, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4327) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Aut aut error et reiciendis.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4377), 12, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4381) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Amet quia minima non nostrum eius alias.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4435), 12, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4440) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Vitae architecto minima enim beatae sit deserunt sint aut.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4503), 3, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4509) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Dolorem maiores id vero accusantium consectetur odio et.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4601), 2, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4606) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Quam beatae sit.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4644), 14, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4649) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Et temporibus totam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4688), 3, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4693) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Adipisci eaque cumque aperiam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4736), 7, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4741) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Quis quas vel sed et exercitationem.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4791), 9, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4796) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Consequatur consequatur est ipsam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4839), 2, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4844) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Consectetur veritatis eos sint dignissimos non nostrum cum sed.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4941), 17, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4946) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(4450), "https://s3.amazonaws.com/uifaces/faces/twitter/gregrwilkinson/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(8463) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9044), "https://s3.amazonaws.com/uifaces/faces/twitter/sementiy/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9066) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9098), "https://s3.amazonaws.com/uifaces/faces/twitter/SULiik/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9102) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9171), "https://s3.amazonaws.com/uifaces/faces/twitter/marcomano_/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9176) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9198), "https://s3.amazonaws.com/uifaces/faces/twitter/dvdwinden/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9202) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9221), "https://s3.amazonaws.com/uifaces/faces/twitter/davidhemphill/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9225) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9244), "https://s3.amazonaws.com/uifaces/faces/twitter/andresenfredrik/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9266), "https://s3.amazonaws.com/uifaces/faces/twitter/benefritz/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9289), "https://s3.amazonaws.com/uifaces/faces/twitter/cbracco/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9293) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9311), "https://s3.amazonaws.com/uifaces/faces/twitter/markretzloff/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9315) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9333), "https://s3.amazonaws.com/uifaces/faces/twitter/cggaurav/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9337) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9354), "https://s3.amazonaws.com/uifaces/faces/twitter/yigitpinarbasi/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9359) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9375), "https://s3.amazonaws.com/uifaces/faces/twitter/safrankov/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9379) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9397), "https://s3.amazonaws.com/uifaces/faces/twitter/lingeswaran/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9402) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9420), "https://s3.amazonaws.com/uifaces/faces/twitter/rtyukmaev/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9424) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9442), "https://s3.amazonaws.com/uifaces/faces/twitter/adhiardana/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9446) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9464), "https://s3.amazonaws.com/uifaces/faces/twitter/paulfarino/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9468) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9486), "https://s3.amazonaws.com/uifaces/faces/twitter/arishi_/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9508), "https://s3.amazonaws.com/uifaces/faces/twitter/overcloacked/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9512) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9529), "https://s3.amazonaws.com/uifaces/faces/twitter/illyzoren/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9534) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(6847), "https://picsum.photos/640/480/?image=490", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7810), "https://picsum.photos/640/480/?image=645", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7839) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7863), "https://picsum.photos/640/480/?image=251", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7867) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7885), "https://picsum.photos/640/480/?image=128", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7889) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7906), "https://picsum.photos/640/480/?image=854", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7927), "https://picsum.photos/640/480/?image=504", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7931) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7948), "https://picsum.photos/640/480/?image=106", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7952) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7969), "https://picsum.photos/640/480/?image=207", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7973) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7989), "https://picsum.photos/640/480/?image=907", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7993) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8011), "https://picsum.photos/640/480/?image=524", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8015) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8031), "https://picsum.photos/640/480/?image=59", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8052), "https://picsum.photos/640/480/?image=766", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8056) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8111), "https://picsum.photos/640/480/?image=1011", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8115) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8134), "https://picsum.photos/640/480/?image=436", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8138) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8155), "https://picsum.photos/640/480/?image=165", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8159) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8176), "https://picsum.photos/640/480/?image=526", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8180) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8196), "https://picsum.photos/640/480/?image=186", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8200) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8217), "https://picsum.photos/640/480/?image=756", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8221) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8237), "https://picsum.photos/640/480/?image=1009", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8241) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8258), "https://picsum.photos/640/480/?image=406", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8262) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8184), true, 5, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8188), 2 },
                    { 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8211), false, 17, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8215), 8 },
                    { 12, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8236), true, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8240), 20 },
                    { 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8262), false, 16, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8267), 14 },
                    { 15, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8314), false, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8318), 18 },
                    { 18, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8392), true, 18, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8396), 11 },
                    { 16, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8340), false, 15, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8344), 6 },
                    { 17, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8366), false, 7, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8370), 13 },
                    { 9, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8157), false, 9, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8161), 2 },
                    { 20, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8508), false, 3, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8512), 4 },
                    { 19, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8418), false, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8422), 9 },
                    { 14, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8289), true, 5, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8293), 7 },
                    { 8, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8130), true, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8134), 4 },
                    { 3, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7993), false, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7998), 17 },
                    { 6, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8076), false, 10, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8081), 21 },
                    { 5, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8048), false, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8053), 5 },
                    { 1, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(6743), false, 7, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7431), 18 },
                    { 4, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8022), true, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8026), 12 },
                    { 2, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7937), true, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7956), 5 },
                    { 7, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8103), false, 3, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8108), 18 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "ut", new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(2144), 38, new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(2808) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Vitae quasi nobis cupiditate.
Qui enim architecto quisquam dolores culpa magnam optio.", new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(9751), 24, new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(9781) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Ut et excepturi explicabo optio.
Necessitatibus ut rerum fuga aperiam laudantium qui tempore quia.
Rerum omnis quibusdam debitis.
Blanditiis sed qui est accusamus temporibus accusantium.
Similique amet ipsa.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(144), 24, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(152) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Quisquam doloribus autem repudiandae.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(943), 33, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(958) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, @"Quaerat asperiores voluptates.
Ducimus ut veniam.
Voluptas dolor omnis repellendus consequatur consequatur eum maiores.
Laboriosam voluptatem iusto asperiores exercitationem nobis sit ab non.
Iure rerum et dolores rerum rem aut totam.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(1214), 38, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(1220) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Et suscipit voluptas. Natus laboriosam ex dolorum perspiciatis non assumenda eaque. Numquam quae sapiente enim. Accusantium et saepe saepe illum mollitia facilis. Ipsam voluptatem ut a odit repudiandae omnis nostrum non.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2625), 34, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2639) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "assumenda", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2685), 35, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "voluptas", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2922), 40, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2928) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "possimus", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2964), 26, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2969) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { @"Molestiae rerum dolores aut sint.
Doloremque qui accusantium sunt porro officia qui vero.
Eveniet nobis reprehenderit repellat nemo.
Ipsam placeat eos ab eos quia omnis architecto inventore.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3176), 25, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3182) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Maiores eos sequi sint eos ut aut eaque nobis.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3254), 22, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3258) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Et fugiat vero in.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3310), 34, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3315) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Laborum necessitatibus enim sapiente expedita est.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3371), 30, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3376) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Consectetur perferendis officiis quia quod. Magnam omnis sit est. Nam omnis quibusdam qui molestias quisquam numquam atque. Saepe ratione voluptatem autem magnam aut iste. Et dolores eum quo sed asperiores. Quia maxime impedit.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3603), 23, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3608) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Et porro voluptatibus modi quisquam dolorem molestiae aut.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3668), 36, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3673) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Molestiae quae eligendi dolores officiis.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3726), 38, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3731) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Iure sint laudantium natus qui est omnis aut aspernatur. Magni ratione modi. Qui accusantium nesciunt reprehenderit iste cupiditate id.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3880), 39, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3885) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 5, "Molestiae dolore fugiat. Et facilis laborum dicta voluptas. Omnis neque iusto. Laboriosam placeat laboriosam esse quia. Qui dicta aut pariatur cumque.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4051), new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4056) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Eligendi animi asperiores impedit dolor sint.
Nulla ad corrupti provident.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4135), 23, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4140) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Repellat a labore amet doloribus.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4193), 33, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4198) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 19, 13, 34, 9, 740, DateTimeKind.Local).AddTicks(6713), "Jaron_Dickinson@gmail.com", "+CgMD2CHTTpKYcmKqrvrqk90UmaADdkKbI8ZI/KaPQc=", "NH0+z3DKfJOSZuiGcb6ru8jw1me61g4jE/WdT14drRc=", new DateTime(2020, 6, 19, 13, 34, 9, 740, DateTimeKind.Local).AddTicks(7464), "Hailee31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 19, 13, 34, 9, 747, DateTimeKind.Local).AddTicks(7524), "Elyse_Goldner@hotmail.com", "ZKLQeXdC5lJ9Gz1MrdXydn8KcoRdqq8n3C7jxalp0Ok=", "KrK02yJx0wt/LNZI7PUb9sAHU+2E4C67bZYnOxb0yZw=", new DateTime(2020, 6, 19, 13, 34, 9, 747, DateTimeKind.Local).AddTicks(7550), "Stacey_Mosciski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 19, 13, 34, 9, 754, DateTimeKind.Local).AddTicks(6028), "Cole.Schuster79@hotmail.com", "SxJqUgYugOw2vRugQdoQqF5Y31Sshsl3hTI6FfbbyLU=", "HTfbSyXDoYX0oL2Wq8gEhQHiAw7Gw6t+S2SRTYCRtxI=", new DateTime(2020, 6, 19, 13, 34, 9, 754, DateTimeKind.Local).AddTicks(6047), "Cathy59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 761, DateTimeKind.Local).AddTicks(4428), "Yasmine_Mohr49@gmail.com", "oWXHvvsHMYRDwfLu1G3hTINBGN3PU1KJiNKpLBwGQtg=", "Id45DLPHuaq9A2Used3npYSuxTct9vywNeWiYOXXb7E=", new DateTime(2020, 6, 19, 13, 34, 9, 761, DateTimeKind.Local).AddTicks(4442), "Jeramie25" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 13, 34, 9, 769, DateTimeKind.Local).AddTicks(9931), "Laura_OConnell@yahoo.com", "HcLC6m8xQN3bAAgolnSYq7rhNUwy84leP6FgBLdemdg=", "hXTpwws/qxwld+omZ0cUM9TRdF76sf1+ak3T20Rx1+s=", new DateTime(2020, 6, 19, 13, 34, 9, 769, DateTimeKind.Local).AddTicks(9966), "Joanne_Purdy29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 13, 34, 9, 776, DateTimeKind.Local).AddTicks(8333), "Breanne79@hotmail.com", "o0yab/45BgOK+K3HT/ZoBMOLVDicJJpTWnAlsiffRO4=", "+tQG3gvLZh8HJYqriBurqb5Ueh+nHcerhOxKsHoeJl8=", new DateTime(2020, 6, 19, 13, 34, 9, 776, DateTimeKind.Local).AddTicks(8344), "Watson.Donnelly" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 19, 13, 34, 9, 783, DateTimeKind.Local).AddTicks(6609), "Karina_Rice@hotmail.com", "WFGRhd/GNzUVQzsnrA0/5uutb7QfnutFpeFykLsvdeQ=", "YG0xFHOezA8sEd+4SiNOseWYY/gs7jxW+SQx8+w913I=", new DateTime(2020, 6, 19, 13, 34, 9, 783, DateTimeKind.Local).AddTicks(6623), "Sandra.Hessel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 13, 34, 9, 790, DateTimeKind.Local).AddTicks(4843), "Jakayla.Jenkins92@hotmail.com", "aDcMfPdRHrwAASauTRAEHBqXGCSsKB23LytNAgLkdrQ=", "pC6r3Se+keiZEyCERX8WdDrh98mPjfP2Y9COXKqEkSI=", new DateTime(2020, 6, 19, 13, 34, 9, 790, DateTimeKind.Local).AddTicks(4854), "Kiera_Smith6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 797, DateTimeKind.Local).AddTicks(3232), "Madilyn81@hotmail.com", "sDPQ8X/Wr/E87EMQCoDEfNfo7DEuiF/3pexRDwNb6is=", "BV6T6eQqu959ySaw30Ay6rm6hFizRmx8YGA2BGKgJ7Q=", new DateTime(2020, 6, 19, 13, 34, 9, 797, DateTimeKind.Local).AddTicks(3240), "German89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 13, 34, 9, 804, DateTimeKind.Local).AddTicks(1485), "Josh_Volkman66@hotmail.com", "MY4i85zK9gR2ZOfhE0GhBQR1Kqaz3h8zVSPG2w+xY74=", "zF4URcKwPDlB6AD/hScNjnLdLJwEVwrsZzJE79OwcgQ=", new DateTime(2020, 6, 19, 13, 34, 9, 804, DateTimeKind.Local).AddTicks(1494), "Demario.Glover" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 19, 13, 34, 9, 811, DateTimeKind.Local).AddTicks(826), "Cordie94@yahoo.com", "2+1FB23hk/GoebDw5CaC3hkLsgrNFsHl98s6/2EAxaE=", "RvMouP9309O2Ulxx3TDERfV1dYtF16S2FJ9AJ1DYQLA=", new DateTime(2020, 6, 19, 13, 34, 9, 811, DateTimeKind.Local).AddTicks(872), "Anastacio_Windler56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 13, 34, 9, 817, DateTimeKind.Local).AddTicks(9615), "Josephine.Mitchell@yahoo.com", "A+sJ5VBned4Baa7DGGM2x7K1Rxz496NVPtw4b8jTPEY=", "hZD7HBghyyMjcQDQn5B5TYM5mspY0K64+ep8dO8kKFk=", new DateTime(2020, 6, 19, 13, 34, 9, 817, DateTimeKind.Local).AddTicks(9626), "Reese_Hamill" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 19, 13, 34, 9, 824, DateTimeKind.Local).AddTicks(7805), "Heath_Reinger@hotmail.com", "tZF35LIj2QI8+WRCJ4UuYraQ8lL+kzJu8shRg9y5iGg=", "8BUvR961WhF2+QLPX+RqQfu0NEIokkSEELMRN+6GKj8=", new DateTime(2020, 6, 19, 13, 34, 9, 824, DateTimeKind.Local).AddTicks(7816), "Myrtis_Daugherty" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 19, 13, 34, 9, 831, DateTimeKind.Local).AddTicks(6421), "Ansley_Crist@yahoo.com", "7Yp9US8G3JFOU8x7RHe1NzPZbeQC2zi4Fd27LyUc1xo=", "NaZR8IVm+QnB/gaZIHryoUdJ5f3oMuFazF44HgNTA/0=", new DateTime(2020, 6, 19, 13, 34, 9, 831, DateTimeKind.Local).AddTicks(6460), "Mollie_Deckow" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 838, DateTimeKind.Local).AddTicks(5047), "Zula41@gmail.com", "Ki2nt6/ihLfOEoED4e1Mx1MQtSlI0A19T6ySspEQDbE=", "HkddnbKS20EXejjQNFWwJpxpdz3Az61vCk/wFcCAclw=", new DateTime(2020, 6, 19, 13, 34, 9, 838, DateTimeKind.Local).AddTicks(5060), "Sherwood_Turcotte27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 13, 34, 9, 845, DateTimeKind.Local).AddTicks(3276), "Demario.Dach@hotmail.com", "kA9ceWf9OZ8Z6HFYbeJc65FmvcoZFxHUdrdQifMMa9E=", "DOWWl/fe4u2qwnBsBNWJeXm6KE9ryWvE8t1ITzRrNVk=", new DateTime(2020, 6, 19, 13, 34, 9, 845, DateTimeKind.Local).AddTicks(3285), "Lucy_Gusikowski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 19, 13, 34, 9, 852, DateTimeKind.Local).AddTicks(1686), "Johnpaul_Volkman@hotmail.com", "fa27ElZc2bDe9g1SN6phX5eexjagWjp5czwfEUZBHQs=", "lybWrzE1g2hKu6NCqEkkmpi/8IOyoofAKpaggzsiX9E=", new DateTime(2020, 6, 19, 13, 34, 9, 852, DateTimeKind.Local).AddTicks(1714), "Liana92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 19, 13, 34, 9, 859, DateTimeKind.Local).AddTicks(216), "Kaden_Willms6@gmail.com", "P89CM8yeV2rkac+kWUIFWpcy0rJxHgvifUJ56lQmxTc=", "GZEBKsuMwI1Z6jRrHeftipSsUAcRw4CwvQoFTPAJ1Bk=", new DateTime(2020, 6, 19, 13, 34, 9, 859, DateTimeKind.Local).AddTicks(227), "Lizzie.Gerhold" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 19, 13, 34, 9, 865, DateTimeKind.Local).AddTicks(8421), "Jude65@gmail.com", "hF2qK6rHzzf8Sl9qF3ch2k3+lXXHEQJUBPrw0oO5wso=", "VyQLKFB70FiHkbQXP254aV/nwxJF5DfTw3s1+DIkRvs=", new DateTime(2020, 6, 19, 13, 34, 9, 865, DateTimeKind.Local).AddTicks(8431), "Alessia_Dietrich65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 872, DateTimeKind.Local).AddTicks(6665), "Kip.Hermann20@hotmail.com", "Q9VzRLURXuIEi4UCSo2pqDDtr/ftS8E0JO76i+rL5jc=", "gu/MWiMy9mILfZmYWpdAc2BejSjyoaZF6cTYc5UZpLI=", new DateTime(2020, 6, 19, 13, 34, 9, 872, DateTimeKind.Local).AddTicks(6675), "Brooke63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 879, DateTimeKind.Local).AddTicks(4673), "PD6YAXAKOpJ30fROq9TRmizRJG2zFQFMY1fYVD2MsLM=", "/B8OmL4I1rekn1n+fIHJ7THwDZJq9wlIHG/22TGlAqU=", new DateTime(2020, 6, 19, 13, 34, 9, 879, DateTimeKind.Local).AddTicks(4673) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Posts");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 5, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2834), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2838), 15 },
                    { 2, 19, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2313), false, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2330), 10 },
                    { 3, 14, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2365), false, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2369), 18 },
                    { 4, 15, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2394), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2399), 21 },
                    { 5, 15, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2422), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2426), 7 },
                    { 6, 5, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2449), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2453), 21 },
                    { 7, 1, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2488), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2493), 11 },
                    { 8, 12, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2519), false, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2523), 8 },
                    { 9, 8, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2547), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2551), 5 },
                    { 10, 11, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2574), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2578), 3 },
                    { 1, 16, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(1091), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(1799), 17 },
                    { 12, 4, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2628), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2632), 10 },
                    { 13, 1, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2656), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2661), 1 },
                    { 14, 8, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2682), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2686), 12 },
                    { 15, 2, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2707), false, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2711), 4 },
                    { 16, 19, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2733), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2737), 21 },
                    { 17, 20, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2759), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2763), 14 },
                    { 18, 5, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2784), false, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2788), 11 },
                    { 19, 9, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2810), false, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2814), 6 },
                    { 11, 2, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2601), true, new DateTime(2020, 6, 19, 1, 15, 55, 939, DateTimeKind.Local).AddTicks(2605), 14 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Sint quisquam quia autem odio.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(5186), 5, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(5819) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Molestiae labore et similique aspernatur non deserunt voluptatum.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6347), 20, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6363) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Delectus dolor natus molestias rerum reprehenderit iure dolores libero aut.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6499), 2, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6507) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Error voluptatem ex consequatur soluta sint quisquam praesentium.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6576), 17, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6581) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Rem tempora earum omnis aut id non iusto qui.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6645), 9, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6650) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Ex numquam tempora maiores.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6696), 14, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6702) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Eos officia amet repellendus consequatur dolor sunt.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6763), 16, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6768) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Quaerat ducimus non ea facere eaque quisquam qui.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6829), 13, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6835) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Ex in totam ut sequi quia repellat mollitia qui recusandae.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6973), 17, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(6979) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "A eius veniam odio.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7026), 20, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7031) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Tenetur qui assumenda voluptas commodi.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7083), 16, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7088) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quia quas sed dignissimos ipsa et quas.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7144), 15, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7149) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Pariatur id in amet nisi minus deserunt facilis aut in.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7216), 16, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7221) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Sint laborum corporis aut fuga.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7270), 3, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7275) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "A reprehenderit qui quam rerum qui modi.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7363), 12, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7368) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Doloremque error iste dicta reprehenderit minus.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7420), 8, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7425) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Minus quisquam amet.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7464), 2, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7469) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Modi voluptatem eum quod et aut.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7519), 12, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7524) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Necessitatibus numquam dolor assumenda in.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7569), 8, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7575) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Odio rerum molestias adipisci qui molestiae modi similique quia labore.", new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7634), 9, new DateTime(2020, 6, 19, 1, 15, 55, 927, DateTimeKind.Local).AddTicks(7639) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 732, DateTimeKind.Local).AddTicks(4959), "https://s3.amazonaws.com/uifaces/faces/twitter/andysolomon/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(594) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1339), "https://s3.amazonaws.com/uifaces/faces/twitter/anjhero/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1361) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1393), "https://s3.amazonaws.com/uifaces/faces/twitter/kkusaa/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1398) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1418), "https://s3.amazonaws.com/uifaces/faces/twitter/moynihan/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1422) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1441), "https://s3.amazonaws.com/uifaces/faces/twitter/nehfy/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1446) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1465), "https://s3.amazonaws.com/uifaces/faces/twitter/wearesavas/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1470) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1488), "https://s3.amazonaws.com/uifaces/faces/twitter/olgary/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1493) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1510), "https://s3.amazonaws.com/uifaces/faces/twitter/d00maz/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1515) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1533), "https://s3.amazonaws.com/uifaces/faces/twitter/marshallchen_/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1537) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1556), "https://s3.amazonaws.com/uifaces/faces/twitter/nfedoroff/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1560) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1579), "https://s3.amazonaws.com/uifaces/faces/twitter/dorphern/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1583) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1601), "https://s3.amazonaws.com/uifaces/faces/twitter/toddrew/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1606) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1624), "https://s3.amazonaws.com/uifaces/faces/twitter/jarjan/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1628) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1646), "https://s3.amazonaws.com/uifaces/faces/twitter/aleclarsoniv/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1651) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1669), "https://s3.amazonaws.com/uifaces/faces/twitter/d_kobelyatsky/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1673) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1689), "https://s3.amazonaws.com/uifaces/faces/twitter/moynihan/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1694) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1712), "https://s3.amazonaws.com/uifaces/faces/twitter/chatyrko/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1717) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1830), "https://s3.amazonaws.com/uifaces/faces/twitter/robturlinckx/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1835) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1857), "https://s3.amazonaws.com/uifaces/faces/twitter/abdots/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1862) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1880), "https://s3.amazonaws.com/uifaces/faces/twitter/stefooo/128.jpg", new DateTime(2020, 6, 19, 1, 15, 55, 733, DateTimeKind.Local).AddTicks(1884) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 737, DateTimeKind.Local).AddTicks(9161), "https://picsum.photos/640/480/?image=38", new DateTime(2020, 6, 19, 1, 15, 55, 737, DateTimeKind.Local).AddTicks(9832) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(60), "https://picsum.photos/640/480/?image=640", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(86) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(110), "https://picsum.photos/640/480/?image=1063", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(115) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(134), "https://picsum.photos/640/480/?image=730", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(138) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(156), "https://picsum.photos/640/480/?image=2", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(161) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(178), "https://picsum.photos/640/480/?image=760", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(182) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(199), "https://picsum.photos/640/480/?image=607", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(204) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(220), "https://picsum.photos/640/480/?image=259", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(224) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(241), "https://picsum.photos/640/480/?image=555", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(246) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(343), "https://picsum.photos/640/480/?image=259", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(348) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(368), "https://picsum.photos/640/480/?image=61", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(372) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(389), "https://picsum.photos/640/480/?image=1027", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(393) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(410), "https://picsum.photos/640/480/?image=798", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(414) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(431), "https://picsum.photos/640/480/?image=1062", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(435) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(453), "https://picsum.photos/640/480/?image=275", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(457) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(474), "https://picsum.photos/640/480/?image=391", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(478) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(495), "https://picsum.photos/640/480/?image=776", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(500) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(516), "https://picsum.photos/640/480/?image=906", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(521) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(537), "https://picsum.photos/640/480/?image=218", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(542) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(559), "https://picsum.photos/640/480/?image=789", new DateTime(2020, 6, 19, 1, 15, 55, 738, DateTimeKind.Local).AddTicks(563) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9327), false, 4, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9332), 13 },
                    { 11, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9354), true, 20, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9359), 14 },
                    { 12, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9424), true, 3, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9429), 12 },
                    { 13, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9452), true, 16, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9457), 18 },
                    { 15, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9503), false, 8, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9507), 11 },
                    { 18, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9600), true, 18, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9604), 6 },
                    { 16, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9528), true, 11, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9533), 5 },
                    { 17, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9572), true, 1, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9577), 1 },
                    { 9, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9302), false, 10, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9306), 5 },
                    { 20, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9649), false, 16, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9653), 17 },
                    { 19, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9624), true, 1, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9629), 21 },
                    { 14, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9477), false, 20, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9481), 21 },
                    { 8, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9277), false, 9, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9281), 16 },
                    { 3, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9134), true, 1, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9139), 9 },
                    { 6, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9221), true, 20, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9226), 12 },
                    { 5, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9194), false, 4, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9199), 5 },
                    { 1, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(7985), false, 1, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(8625), 17 },
                    { 4, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9164), false, 10, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9169), 12 },
                    { 2, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9084), true, 15, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9098), 21 },
                    { 7, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9248), false, 8, new DateTime(2020, 6, 19, 1, 15, 55, 933, DateTimeKind.Local).AddTicks(9253), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Unde ut expedita ab doloremque nemo voluptas.", new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(2366), 29, new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(3294) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, @"Ab et enim id dolores et.
Aut quaerat ad aliquam quod tempora doloremque praesentium deserunt laborum.
Rerum ut qui et autem in nihil ducimus.
Alias deleniti deserunt.", new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(7478), 40, new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(7507) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Eos suscipit cumque sit consectetur. Est magnam enim veniam sint qui repellat pariatur magnam. Et sapiente inventore voluptas maiores consequatur minus sit est.", new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(8955), 31, new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(8971) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Corporis dignissimos repellendus amet nihil quia quaerat repudiandae.", new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(9108), 34, new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(9114) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, @"Cupiditate nostrum molestias fuga et sequi tempora dolor et.
Saepe sed nisi magni animi sint voluptas facilis consequatur.
Qui ut voluptas magni quidem.
Non asperiores saepe quis excepturi.
Et ipsum atque odio cum veritatis deleniti.
Explicabo rerum qui mollitia necessitatibus maiores ea consectetur expedita.", new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(9421), 28, new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(9428) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Tempora omnis qui vel doloribus assumenda et. Necessitatibus provident suscipit iure. Quia facilis nesciunt voluptas ut. Aut tempora cumque sunt. Labore est itaque suscipit.", new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(9615), 27, new DateTime(2020, 6, 19, 1, 15, 55, 920, DateTimeKind.Local).AddTicks(9621) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "impedit", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(140), 36, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(154) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Exercitationem architecto impedit quia eos repellat et quaerat quas est. Perspiciatis dolores omnis aspernatur. Voluptas eum cupiditate reiciendis non eos laboriosam fugiat. Eveniet iure cupiditate et dolorum. Incidunt quo quia perspiciatis consectetur qui assumenda consequatur nihil impedit. Minus a facere temporibus voluptas rerum voluptates veritatis alias repudiandae.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(519), 29, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(527) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Debitis repudiandae delectus est nisi harum rerum est. Fugiat quia aut voluptates consectetur asperiores perferendis qui enim modi. Vel vitae et illo dignissimos laborum.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(704), 32, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(710) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "illum", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(743), 36, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(748) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "In vero ea aspernatur consequatur ex.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(804), 38, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(810) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Recusandae repellat officiis consequatur eveniet mollitia nobis. Facere ex quibusdam velit voluptates neque qui. Voluptatem id eos. Molestiae dolorum blanditiis. Eos laborum quisquam et accusamus iste aut. Ut sed molestias et eligendi.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1032), 33, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1039) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "architecto", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1071), 37, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1076) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "omnis", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1105), 29, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1110) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Officia nam nam rerum deserunt. Perspiciatis aliquam et quis debitis. Nobis adipisci dolore sint accusamus quis et fugit autem. Quam sint repellat dicta voluptatibus ea quo eligendi. Dolor ut culpa.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1347), 40, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1354) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, @"Adipisci quasi iusto placeat ut nisi exercitationem sequi odio at.
Sunt sed quis cupiditate repudiandae.
Dolore maxime necessitatibus molestias sit amet esse ipsum dolorem explicabo.
Odit tempora quibusdam ut numquam molestiae blanditiis.
Itaque eveniet occaecati.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1568), 32, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1574) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "In sed alias dolores maxime libero doloremque. Accusamus voluptas iusto eos. Voluptatem in minus magnam repellendus ab numquam illo commodi ex. Eum deleniti aut eaque qui ipsum consectetur culpa. Cum quo ab dolorem mollitia assumenda iusto adipisci mollitia.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1789), 21, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(1795) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 14, "Sint quia repellendus quia. Debitis voluptas blanditiis numquam voluptas enim libero voluptatem optio doloribus. Cumque sunt vero architecto quia sit. Similique et perferendis et sit blanditiis sit. Optio ut quia illo porro earum nulla ad cumque praesentium.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(2006), new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(2013) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Non molestias quod sed quia sit aut dolores occaecati.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(2105), 39, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(2111) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Eveniet voluptatem enim repellat aut eos laborum et. Rerum ad provident at exercitationem nesciunt laborum voluptatibus natus ut. Est molestias debitis vel sit alias voluptatem natus.", new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(2285), 35, new DateTime(2020, 6, 19, 1, 15, 55, 921, DateTimeKind.Local).AddTicks(2292) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 19, 1, 15, 55, 771, DateTimeKind.Local).AddTicks(3092), "Marcellus_Bergnaum72@yahoo.com", "Q7NjLuEe3C/oPHOPNa7PjxfD7/lmw57Od9OrB3ER8GQ=", "uk9LX83k7hOJQwlWTbqQEaeBlEiMkkH1m5TebCjnC6M=", new DateTime(2020, 6, 19, 1, 15, 55, 771, DateTimeKind.Local).AddTicks(3869), "Arvilla_Kling67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 19, 1, 15, 55, 778, DateTimeKind.Local).AddTicks(4884), "Davon.McClure99@yahoo.com", "9wyWU+fiDbMEcUfoLYjeaReq1sGlnIaXqfNxzkDUfxA=", "qkf3ebTDy/vZGrTuf6cl9eM6RU50RTq+hORwDJExkhA=", new DateTime(2020, 6, 19, 1, 15, 55, 778, DateTimeKind.Local).AddTicks(4918), "Gloria.Cartwright33" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 19, 1, 15, 55, 785, DateTimeKind.Local).AddTicks(3597), "Dovie58@hotmail.com", "Pevh0y0ESKM/2f0bpkYz95UVuBGICsMdEgDnZ2FLXvw=", "dyZdoZz9ZW8qTAku2fMC26LeeZT37/BMUcSxo7pXzxg=", new DateTime(2020, 6, 19, 1, 15, 55, 785, DateTimeKind.Local).AddTicks(3610), "Winifred43" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 792, DateTimeKind.Local).AddTicks(2376), "Hollie51@hotmail.com", "1BZbhxy487HTNSc9RT8U9fBgJyRNOCBIIhCB79FZLcA=", "/JT70LnB3z2rML/NSdnKXaKUTkCY1lO65e2aP58v+cw=", new DateTime(2020, 6, 19, 1, 15, 55, 792, DateTimeKind.Local).AddTicks(2393), "Jeramy.Reichert38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 19, 1, 15, 55, 799, DateTimeKind.Local).AddTicks(861), "Genoveva54@gmail.com", "BDb8wFk7O4tkpf2uwbT23213zYMeFSb3VZ8GKynVCww=", "zmr+iL0BOX4MupdB9lL3WMg9Tf206TB0Kvzt3pPO10o=", new DateTime(2020, 6, 19, 1, 15, 55, 799, DateTimeKind.Local).AddTicks(871), "Nathan.Harris" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 19, 1, 15, 55, 806, DateTimeKind.Local).AddTicks(1694), "Lonnie.MacGyver50@gmail.com", "dl/0cn5WDZvjNGO/AloD7e6B6C8wsS0bJtygAqLogTc=", "PWRIrzn/J+74jkUB4MTErheTJJp0AXRJN+oyYmPj8/0=", new DateTime(2020, 6, 19, 1, 15, 55, 806, DateTimeKind.Local).AddTicks(1767), "Cecelia70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 19, 1, 15, 55, 813, DateTimeKind.Local).AddTicks(1093), "Shayna.Leffler@gmail.com", "erIg/mw4Ibam8B87zExHATXKGJrOMofiiy538ly10WA=", "emO5IbhQ63ZtAkaCchiD1andGASkNU+obVTbREvGMj0=", new DateTime(2020, 6, 19, 1, 15, 55, 813, DateTimeKind.Local).AddTicks(1115), "Hanna_Little56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 19, 1, 15, 55, 819, DateTimeKind.Local).AddTicks(9961), "Christiana_Parisian65@gmail.com", "M/4F2WpAEfrkrQkIC8VkmifltJ4CRVPnNI0Pc6GalIw=", "aG43TmHyCbA1HE4MPwsVLAxdDkwy19YcFoUDxWjpMxU=", new DateTime(2020, 6, 19, 1, 15, 55, 819, DateTimeKind.Local).AddTicks(9971), "Ford_Nolan29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 826, DateTimeKind.Local).AddTicks(8958), "Emory.Yundt@yahoo.com", "vmYy++c7nyRE5r3uBP5LZb+SPV/UmEt8+RtvQDqo3zQ=", "PuaRTxpDYR3WEIrg+ZrNgt7T8b+0zmAfJjSbTK9ByTY=", new DateTime(2020, 6, 19, 1, 15, 55, 826, DateTimeKind.Local).AddTicks(8974), "Salvatore_Fisher18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 19, 1, 15, 55, 833, DateTimeKind.Local).AddTicks(7485), "Maude83@yahoo.com", "F7oH15hLGrUglLFqdzZuaJ7DO9bTHH4QmwtVMk5cII8=", "8qngtNntnLj30cmr5LmZORHaiddEXc/wOPdaOh04PhQ=", new DateTime(2020, 6, 19, 1, 15, 55, 833, DateTimeKind.Local).AddTicks(7497), "Arlie.Borer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 19, 1, 15, 55, 840, DateTimeKind.Local).AddTicks(6227), "Adolfo1@hotmail.com", "wS+7f5F8dqnHoj7RKPpT3XqZQI3sl4ouXfTwAYzQaeY=", "NaVL90kJSNt7GRVsLe0A+UHMCpFYd2xIIgjW3jASRVA=", new DateTime(2020, 6, 19, 1, 15, 55, 840, DateTimeKind.Local).AddTicks(6244), "Deangelo_Treutel91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 1, 15, 55, 847, DateTimeKind.Local).AddTicks(4751), "Darius82@yahoo.com", "wVks9Z+X4V+Pdq9WKBNtZmwNC7biSbYOaFHq57iSKfU=", "zFJiL2LpO1UNhyInbn38p/goqgIbLB8EOvX3RryJ3Z4=", new DateTime(2020, 6, 19, 1, 15, 55, 847, DateTimeKind.Local).AddTicks(4763), "Blanche.Dickinson67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 19, 1, 15, 55, 854, DateTimeKind.Local).AddTicks(6743), "Alexandro.Huel78@hotmail.com", "xFNrLh20IbXJEnPXh/rBTsIu8Y0UUDd1XFsO5ADfah0=", "M3D48t0LwC7/GzU3FrZqvnxciYT1rlNm3A+jn7VWATU=", new DateTime(2020, 6, 19, 1, 15, 55, 854, DateTimeKind.Local).AddTicks(6757), "Easton82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 1, 15, 55, 861, DateTimeKind.Local).AddTicks(5090), "Leslie_Luettgen@yahoo.com", "tnL9+HPnjqp2+od9v2A9cYQhsGAi9ugaZC7ZSRjTrMo=", "g0Yz+CTltMtU6Fnrr1i67GXURCucgWXFCP3hSm22Ggc=", new DateTime(2020, 6, 19, 1, 15, 55, 861, DateTimeKind.Local).AddTicks(5100), "Boyd35" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 868, DateTimeKind.Local).AddTicks(3612), "Velva.Keeling@yahoo.com", "tDAdUpXkPtpOyguU0t2Bi3Qs6AchXQi3wOgv5CE4fz8=", "oFAu3zQZIt5GECwJgT9hzJxgOO1OJDyX9mDiHIwEIog=", new DateTime(2020, 6, 19, 1, 15, 55, 868, DateTimeKind.Local).AddTicks(3625), "Aurelio.Glover" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 19, 1, 15, 55, 875, DateTimeKind.Local).AddTicks(2016), "Marcos_Nikolaus@yahoo.com", "Gl1MbsY0PGI9/i/8mIBmlQjBmzz5shDau3U07s66uKI=", "B20Lfr8N/QPq+MJSC5Kg2F4nJKFgx5Ec3OAJcIG0By8=", new DateTime(2020, 6, 19, 1, 15, 55, 875, DateTimeKind.Local).AddTicks(2027), "Kailey9" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 19, 1, 15, 55, 882, DateTimeKind.Local).AddTicks(458), "Rey_Leannon@gmail.com", "krxg4FxP9d32QIcDMu9X0LgnB4AAtVEAk9u+NtWWhx8=", "yHIzc0JFfxGaCQQ79Vf/2IF6Rox2l1q2xkvQs2C1Llw=", new DateTime(2020, 6, 19, 1, 15, 55, 882, DateTimeKind.Local).AddTicks(468), "Jennifer99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 19, 1, 15, 55, 888, DateTimeKind.Local).AddTicks(8875), "Buddy.Abernathy@yahoo.com", "kdH9oF1MSoDjBZ9omNTtWCghEWMf7QEacx9clWVTuDI=", "Oztzsn3cAduLHGXWrVot3LIRmrLZdoGURGFfa9n47YE=", new DateTime(2020, 6, 19, 1, 15, 55, 888, DateTimeKind.Local).AddTicks(8887), "Dena_Hane" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 1, 15, 55, 895, DateTimeKind.Local).AddTicks(7261), "Osvaldo58@hotmail.com", "97R5Z8t83VcqvxqqNZtO4R8sYY5xE2PKrjAG5rPK73c=", "rtDixQTinM+2CkYdVx07A+OvBF5lUveqVK2G7dczPi8=", new DateTime(2020, 6, 19, 1, 15, 55, 895, DateTimeKind.Local).AddTicks(7273), "Tod6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 902, DateTimeKind.Local).AddTicks(5786), "Maureen.Jerde@gmail.com", "D5GxzaT0dqVAto46mLzeTrZfCVA6dW6z0yZZoOfIf4w=", "Icj838a+HdfqzUjmJKNvX766rcVynrHDWs8Y+pLnujA=", new DateTime(2020, 6, 19, 1, 15, 55, 902, DateTimeKind.Local).AddTicks(5803), "Braulio67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 1, 15, 55, 909, DateTimeKind.Local).AddTicks(4125), "IlHNI6vDspzpWJW39UIo3wJ3AEZ1PsRsrZ/V6sRdEWY=", "1/ZHZ86K7R5r6n3GLNpW6DfLDA1TOtrrCTZ3yabdxEM=", new DateTime(2020, 6, 19, 1, 15, 55, 909, DateTimeKind.Local).AddTicks(4125) });
        }
    }
}
