﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddIsDeletedToComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 16, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3335), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3339), 3 },
                    { 2, 8, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2807), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2838), 13 },
                    { 3, 8, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2871), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2876), 11 },
                    { 4, 11, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2900), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2905), 6 },
                    { 5, 7, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2928), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2933), 3 },
                    { 6, 13, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2955), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2960), 16 },
                    { 7, 20, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2983), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2987), 7 },
                    { 8, 20, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3009), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3013), 11 },
                    { 9, 8, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3035), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3040), 17 },
                    { 10, 20, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3060), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3064), 6 },
                    { 1, 6, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(1564), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(2200), 11 },
                    { 12, 13, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3112), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3116), 17 },
                    { 13, 14, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3138), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3143), 15 },
                    { 14, 12, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3166), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3170), 21 },
                    { 15, 2, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3192), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3196), 17 },
                    { 16, 20, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3217), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3221), 14 },
                    { 17, 7, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3243), false, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3247), 12 },
                    { 18, 11, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3268), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3272), 15 },
                    { 19, 1, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3307), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3312), 10 },
                    { 11, 19, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3086), true, new DateTime(2020, 6, 19, 20, 17, 1, 270, DateTimeKind.Local).AddTicks(3091), 16 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Harum nihil laborum.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(588), 19, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1222) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Debitis itaque voluptatem dolorem.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1744), 19, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1758) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Voluptas porro dolorum eaque sint.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1827), 4, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1832) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Eum eos quasi eius officiis hic expedita et quia ipsa.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1906), 13, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1911) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Commodi rerum voluptatem cupiditate.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1962), 9, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(1966) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Occaecati neque dolorem velit rem.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2065), 17, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2071) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut iste optio eos.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2119), 5, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2123) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Nulla perspiciatis dolorem illo eius modi molestias illo quia.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2188), 7, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2193) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Sapiente est ullam sint.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2240), 19, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2245) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Iure libero eligendi expedita maxime est ratione deleniti omnis est.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2312), 19, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2317) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Perferendis error aut omnis optio quia modi illo.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2378), 3, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2383) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Adipisci sequi dicta.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2492), 9, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2498) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Ipsa sapiente quia explicabo eum dolores voluptatem.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2556), 2, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2561) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Architecto quod tenetur ea dolor ullam.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2616), 3, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2621) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Tempore cupiditate illum et impedit qui.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2676), 12, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2681) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Laudantium deserunt natus corporis beatae ut est eum.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2745), 12, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2749) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Molestiae hic necessitatibus dolorum.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2793), 3, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2797) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 21, "Inventore nihil voluptatem eveniet quos.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2878), new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2882) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Inventore modi ut dolores ad blanditiis in quas.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2944), 10, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(2949) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Assumenda nam maiores quibusdam qui culpa aut earum ut.", new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(3013), 13, new DateTime(2020, 6, 19, 20, 17, 1, 258, DateTimeKind.Local).AddTicks(3017) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 65, DateTimeKind.Local).AddTicks(6683), "https://s3.amazonaws.com/uifaces/faces/twitter/derekcramer/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(770) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1334), "https://s3.amazonaws.com/uifaces/faces/twitter/claudioguglieri/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1359) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1392), "https://s3.amazonaws.com/uifaces/faces/twitter/victorDubugras/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1397) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1418), "https://s3.amazonaws.com/uifaces/faces/twitter/brandclay/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1423) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1442), "https://s3.amazonaws.com/uifaces/faces/twitter/diesellaws/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1446) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1466), "https://s3.amazonaws.com/uifaces/faces/twitter/bistrianiosip/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1470) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1488), "https://s3.amazonaws.com/uifaces/faces/twitter/hiemil/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1493) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1511), "https://s3.amazonaws.com/uifaces/faces/twitter/shinze/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1516) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1535), "https://s3.amazonaws.com/uifaces/faces/twitter/adamawesomeface/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1539) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1557), "https://s3.amazonaws.com/uifaces/faces/twitter/jjshaw14/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1562) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1578), "https://s3.amazonaws.com/uifaces/faces/twitter/keryilmaz/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1583) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1602), "https://s3.amazonaws.com/uifaces/faces/twitter/samuelkraft/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1607) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1624), "https://s3.amazonaws.com/uifaces/faces/twitter/trubeatto/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1629) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1647), "https://s3.amazonaws.com/uifaces/faces/twitter/bighanddesign/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1651) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1668), "https://s3.amazonaws.com/uifaces/faces/twitter/patrickcoombe/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1673) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1736), "https://s3.amazonaws.com/uifaces/faces/twitter/nelshd/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1742) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1763), "https://s3.amazonaws.com/uifaces/faces/twitter/pierrestoffe/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1767) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1784), "https://s3.amazonaws.com/uifaces/faces/twitter/craighenneberry/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1788) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1807), "https://s3.amazonaws.com/uifaces/faces/twitter/ryankirkman/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1812) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1827), "https://s3.amazonaws.com/uifaces/faces/twitter/jjshaw14/128.jpg", new DateTime(2020, 6, 19, 20, 17, 1, 66, DateTimeKind.Local).AddTicks(1832) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(8621), "https://picsum.photos/640/480/?image=984", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9306) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9541), "https://picsum.photos/640/480/?image=903", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9566) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9592), "https://picsum.photos/640/480/?image=1072", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9597) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9616), "https://picsum.photos/640/480/?image=82", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9621) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9639), "https://picsum.photos/640/480/?image=556", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9643) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9661), "https://picsum.photos/640/480/?image=164", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9665) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9684), "https://picsum.photos/640/480/?image=228", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9688) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9706), "https://picsum.photos/640/480/?image=1059", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9710) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9773), "https://picsum.photos/640/480/?image=740", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9778) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9798), "https://picsum.photos/640/480/?image=248", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9802) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9820), "https://picsum.photos/640/480/?image=137", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9825) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9843), "https://picsum.photos/640/480/?image=415", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9847) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9865), "https://picsum.photos/640/480/?image=378", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9870) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9887), "https://picsum.photos/640/480/?image=586", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9892) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9910), "https://picsum.photos/640/480/?image=351", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9914) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9932), "https://picsum.photos/640/480/?image=1002", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9936) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9954), "https://picsum.photos/640/480/?image=940", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9959) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9976), "https://picsum.photos/640/480/?image=681", new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9981) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 70, DateTimeKind.Local).AddTicks(9998), "https://picsum.photos/640/480/?image=175", new DateTime(2020, 6, 19, 20, 17, 1, 71, DateTimeKind.Local).AddTicks(3) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 71, DateTimeKind.Local).AddTicks(21), "https://picsum.photos/640/480/?image=311", new DateTime(2020, 6, 19, 20, 17, 1, 71, DateTimeKind.Local).AddTicks(25) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8640), false, 8, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8644), 7 },
                    { 11, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8666), true, 1, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8671), 10 },
                    { 12, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8693), true, 9, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8698), 1 },
                    { 13, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8719), false, 3, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8723), 17 },
                    { 15, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8771), true, 19, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8775), 6 },
                    { 18, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8846), true, 6, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8850), 3 },
                    { 16, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8795), true, 9, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8799), 2 },
                    { 17, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8821), true, 17, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8825), 8 },
                    { 9, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8615), false, 19, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8620), 2 },
                    { 20, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8895), true, 5, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8900), 19 },
                    { 19, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8871), true, 1, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8875), 2 },
                    { 14, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8745), false, 18, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8750), 3 },
                    { 8, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8589), true, 8, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8593), 19 },
                    { 3, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8437), true, 2, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8442), 12 },
                    { 6, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8536), true, 15, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8541), 6 },
                    { 5, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8508), false, 1, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8512), 9 },
                    { 1, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(7277), true, 12, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(7914), 19 },
                    { 4, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8465), false, 7, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8469), 19 },
                    { 2, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8387), false, 18, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8402), 7 },
                    { 7, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8563), false, 10, new DateTime(2020, 6, 19, 20, 17, 1, 264, DateTimeKind.Local).AddTicks(8567), 10 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Illo veritatis animi hic.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(1583), 40, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(2286) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Ut facere id et repellat saepe aliquid est dicta eum. Voluptatibus sit velit et voluptatem et doloremque esse architecto nobis. Error voluptatem et ut cupiditate quo earum cum.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(6545), 31, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(6574) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Dolorem ut autem itaque et quo nam et. Ut ut eaque est hic et autem natus laudantium. Ullam eius amet. Nostrum voluptas consequatur excepturi fugiat. Aut enim architecto nemo nam atque.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(6916), 29, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(6926) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "omnis", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7445), 28, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7459) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "eligendi", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7507), 40, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7512) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "debitis", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7544), 38, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7549) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Dolore nulla velit non voluptatibus laborum quas quia dolores sed.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7638), 37, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7644) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Qui maiores laudantium non. Totam voluptates cumque minima consectetur quos ipsam. Similique corrupti dolorum occaecati aut itaque voluptatem incidunt saepe eos.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7831), 31, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7838) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Illo nesciunt amet temporibus nihil saepe eius totam saepe molestiae.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7907), 23, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7913) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Qui magni suscipit corrupti exercitationem.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7962), 32, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(7968) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "culpa", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8001), 27, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8006) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "illo", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8035), 39, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8040) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Ut unde at perspiciatis explicabo ea. Nihil est aspernatur mollitia adipisci quam commodi laudantium nobis. Omnis et perferendis. Est nemo quae in debitis expedita et. Nostrum et quae totam optio molestias molestiae atque animi. Non cumque officiis.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8287), 39, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8293) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Saepe voluptas officia quis voluptas architecto.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8393), 21, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8400) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Consequatur delectus numquam placeat. Officia in totam omnis. Quisquam vero sed sed atque est officiis enim. Qui et quidem consectetur voluptatibus saepe veritatis totam. Iste alias occaecati aut repellat nobis.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8588), 34, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8595) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Veniam quas eaque vero distinctio quia voluptates. Dolorem ea ut. Quidem aliquam est odio.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8733), 24, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8739) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "at", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8771), 22, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(8776) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Quia repudiandae vero pariatur non pariatur ut. Maxime ex ipsam omnis omnis non consequatur voluptatibus. Ullam sunt quia. Rerum unde nemo et quas vero officia voluptas quos. Iste consequuntur enim qui. Ab sed rerum molestias ducimus.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(9005), 30, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(9012) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Qui dolore molestiae accusamus et dolorem eos nihil. Quia consequatur omnis id accusantium doloribus ipsam ea. Adipisci omnis quidem totam beatae. Recusandae aut expedita culpa omnis aliquid repellat.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(9223), 33, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(9231) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, @"Nesciunt blanditiis nihil suscipit eligendi quam deserunt.
Saepe et mollitia et.", new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(9936), 31, new DateTime(2020, 6, 19, 20, 17, 1, 251, DateTimeKind.Local).AddTicks(9951) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 20, 17, 1, 104, DateTimeKind.Local).AddTicks(5591), "Cruz_Spencer22@gmail.com", "ci0/f3/0jy9N5Q+g31pSfL3nxQd/p8OI0irzAWzlI3E=", "hZFpP/OzaYKpC2cuxaYxzAPRqXVqUM1hPHUDwQj6Xa8=", new DateTime(2020, 6, 19, 20, 17, 1, 104, DateTimeKind.Local).AddTicks(6315), "Breanna_Stark" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 20, 17, 1, 111, DateTimeKind.Local).AddTicks(6188), "Cordie.Hintz31@gmail.com", "HHWe3SXO5/7qawoqlRvrv0Fa7cvh0srf2iod98PLcmM=", "GyYrD3XZj2Z8N1xSSQa/MIvZqQxVkECZE+5SDq1qonM=", new DateTime(2020, 6, 19, 20, 17, 1, 111, DateTimeKind.Local).AddTicks(6214), "Sonya_Nolan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 19, 20, 17, 1, 118, DateTimeKind.Local).AddTicks(4904), "Toy.Ferry@yahoo.com", "wNhfHARvh7Z7C+aiKvEx8s/2Qsh69GI0z3x0E2sbQ1Y=", "SVwmgyIi+qgSGtQKxf3LHGsOhCfEVfRe6uDWXxiLhsA=", new DateTime(2020, 6, 19, 20, 17, 1, 118, DateTimeKind.Local).AddTicks(4934), "Daniela4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 19, 20, 17, 1, 125, DateTimeKind.Local).AddTicks(3156), "Myrtie2@gmail.com", "fbFjy69qNHgNmDO1nVfzaCMJi/cA1m7XtxYegRe9zNo=", "ceLrdiNnjMhnbGMqtN4w80QlgyoezZX+vIF5Hd9Ye+E=", new DateTime(2020, 6, 19, 20, 17, 1, 125, DateTimeKind.Local).AddTicks(3170), "Elwin.Braun" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 19, 20, 17, 1, 132, DateTimeKind.Local).AddTicks(1453), "Buster.VonRueden62@gmail.com", "OSmFd7Xr+jLdHWOOuoamm6pYsRYpiGZNzJIOq2oPQqY=", "RJu9KWEhLkbtEATRTSAQOi555by/P12guVokzqzY0DA=", new DateTime(2020, 6, 19, 20, 17, 1, 132, DateTimeKind.Local).AddTicks(1465), "Edgar.Toy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 20, 17, 1, 138, DateTimeKind.Local).AddTicks(9545), "Sigurd_Dibbert94@hotmail.com", "dz/Efnv+61icyGBzCCtHaadPHAcSO3d8tKB1AJ8wk6M=", "NiPjhx10gKwRbez4weoCdHGu8xClfYtAdtoxK2MA4Og=", new DateTime(2020, 6, 19, 20, 17, 1, 138, DateTimeKind.Local).AddTicks(9555), "Theron.Beatty22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 20, 17, 1, 145, DateTimeKind.Local).AddTicks(7877), "Euna_Goyette@hotmail.com", "QJBiR8s8P2t4yJb6sNjhUxw7sgaWfgzJNMJXPEisOfU=", "I2anSFcAoGBjLrjLKu7Vq5UnDlYDs1FPdAZjNHPx+U4=", new DateTime(2020, 6, 19, 20, 17, 1, 145, DateTimeKind.Local).AddTicks(7888), "Josh.Price6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 20, 17, 1, 152, DateTimeKind.Local).AddTicks(6049), "Henriette_Beatty44@hotmail.com", "SmKRHRA07jCW4iRsPlXHuTS8HYVuSZGUxLBMbANgkZk=", "70ElpBOLXtpRjhodVluTSbsfvhTDvlVLDVSILz1T/GM=", new DateTime(2020, 6, 19, 20, 17, 1, 152, DateTimeKind.Local).AddTicks(6066), "Finn_VonRueden62" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 19, 20, 17, 1, 159, DateTimeKind.Local).AddTicks(4669), "Luna.Green63@gmail.com", "uONdJNdH3YUmevgpZb+0yervvKyG5Nt3xvEgvFdns08=", "mCS9svoOv+eiHTACZgE43UJniMR/4/xsuRos0934Caw=", new DateTime(2020, 6, 19, 20, 17, 1, 159, DateTimeKind.Local).AddTicks(4720), "Ova61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 20, 17, 1, 166, DateTimeKind.Local).AddTicks(3177), "Lucas.Metz@hotmail.com", "kVtXHEnxhRONBli7yVbANL5gOqVjs7HbzwIkr98I3IQ=", "jHDiMiJ2plnDa0WfuuXCRTTHFnqWVT1th8jvG8rs4kU=", new DateTime(2020, 6, 19, 20, 17, 1, 166, DateTimeKind.Local).AddTicks(3204), "Pearl_Cremin19" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 19, 20, 17, 1, 173, DateTimeKind.Local).AddTicks(1528), "Litzy_Blanda@hotmail.com", "1mHo9OKqoVorQFc9J/pLD0FznNiPcvAQ7iYj+OLgljs=", "TKmYO3VE22qt3Syl6/IfSzS04gqjALsCjM9PGyN1oTc=", new DateTime(2020, 6, 19, 20, 17, 1, 173, DateTimeKind.Local).AddTicks(1553), "Steve28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 19, 20, 17, 1, 180, DateTimeKind.Local).AddTicks(29), "Elmira_Auer@gmail.com", "lZN3QfEmVDqCuyOUvjDO2Akd0QdZIAnpARO+VRjid1A=", "faTz71Ecc5zxvEMOPJb1Ve1g1X1rP0YD907XeE3R8O4=", new DateTime(2020, 6, 19, 20, 17, 1, 180, DateTimeKind.Local).AddTicks(56), "Kayden_Stark76" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 19, 20, 17, 1, 186, DateTimeKind.Local).AddTicks(8486), "Obie_Considine@hotmail.com", "BB55nBArqgaJgytxuFaw02SmLd/iSf3NfS+EbEwT3i8=", "KctwJ12FRqT0XHB1XyH9EPoK59qXSOWo2OhUn1egZ3o=", new DateTime(2020, 6, 19, 20, 17, 1, 186, DateTimeKind.Local).AddTicks(8496), "Brandyn_Schneider" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 19, 20, 17, 1, 193, DateTimeKind.Local).AddTicks(6834), "Archibald.Reilly74@hotmail.com", "Egf2f055XjfwWrvZqiqeMWuoH1nnu4/2utPEE1FBQtE=", "9NalO6K6nSnKbyjHvL4/OaVVea77JUNMKr54yhO8S8s=", new DateTime(2020, 6, 19, 20, 17, 1, 193, DateTimeKind.Local).AddTicks(6865), "Glen_Reinger84" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 20, 17, 1, 200, DateTimeKind.Local).AddTicks(5097), "Jacinto_Veum@yahoo.com", "y1hnwHUGLSu7afpGZg5eQP0YsyXjtWiE1QzAEoNHBBk=", "CvvR8XpO+Jnas2EyJdMs6Y8Obfn1blDGEFzhtGBceMs=", new DateTime(2020, 6, 19, 20, 17, 1, 200, DateTimeKind.Local).AddTicks(5112), "Jewell37" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 19, 20, 17, 1, 207, DateTimeKind.Local).AddTicks(3226), "Agustin.Roberts66@gmail.com", "aspaDCdilOHF3UZOAjOMx9qB84wkT6Rfhin6fcSGe+s=", "h1rsDIaGL0Qi/53ZkJQD0OLXQHwiEAkx+zNJ4B1W86Y=", new DateTime(2020, 6, 19, 20, 17, 1, 207, DateTimeKind.Local).AddTicks(3235), "Sibyl68" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 20, 17, 1, 214, DateTimeKind.Local).AddTicks(3419), "Carley.Feil@gmail.com", "lLOEzbAtMNtBDXkCYksl/7puiwbl+2M6MChuY/CIF+w=", "8pwMWrtxL8V0EUgR2JhiRrGEp4NsqH5wz2PEEyoi5RY=", new DateTime(2020, 6, 19, 20, 17, 1, 214, DateTimeKind.Local).AddTicks(3485), "Daryl56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 19, 20, 17, 1, 221, DateTimeKind.Local).AddTicks(2146), "Kaitlyn71@yahoo.com", "IXOdvvDx/nxFe2AR+ZIxuN/UsrjmXcfJGpiA8/TaWmk=", "xI3PU/+jeGTv4voL2m6z7WKvJNBPzln2NkseWLdmQI8=", new DateTime(2020, 6, 19, 20, 17, 1, 221, DateTimeKind.Local).AddTicks(2177), "Chyna.Jenkins91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 20, 17, 1, 228, DateTimeKind.Local).AddTicks(516), "Kayleigh10@gmail.com", "1zVP//LY48WucM19xVFc/7UkpEELtRjm/DNI6wiYwK0=", "Hn/lQGq0a6nFpu9us7/ydyjlb+Qaewylx7MapjOFS1Q=", new DateTime(2020, 6, 19, 20, 17, 1, 228, DateTimeKind.Local).AddTicks(538), "Norwood77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 20, 17, 1, 234, DateTimeKind.Local).AddTicks(9375), "Reina.Nolan58@yahoo.com", "Ho7epnq7MBGuTdfamdBEfBJ5lns0Qfs+uYdEQRyrOTA=", "Hp5oTJdZIZ6grSouQzPIUJQnHipA7M8NS3R38fWdmho=", new DateTime(2020, 6, 19, 20, 17, 1, 234, DateTimeKind.Local).AddTicks(9709), "Virginie12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 20, 17, 1, 241, DateTimeKind.Local).AddTicks(8222), "PN7nWtHF1pECHdPb+Q9mWpAGDCAN8pUd/GVQR9O9Vuw=", "UBXPAkkpMnUADIeLR9ijvSUxsai9orFPNNnVYQUmJvs=", new DateTime(2020, 6, 19, 20, 17, 1, 241, DateTimeKind.Local).AddTicks(8222) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 18, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3301), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3305), 9 },
                    { 2, 5, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2768), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2783), 8 },
                    { 3, 6, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2818), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2823), 10 },
                    { 4, 7, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2847), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2852), 16 },
                    { 5, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2876), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2881), 12 },
                    { 6, 8, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2902), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2906), 12 },
                    { 7, 17, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2927), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2931), 9 },
                    { 8, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2953), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2957), 5 },
                    { 9, 18, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3026), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3031), 11 },
                    { 10, 11, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3052), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3056), 16 },
                    { 1, 15, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(1638), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(2316), 21 },
                    { 12, 7, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3102), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3106), 2 },
                    { 13, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3127), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3132), 6 },
                    { 14, 16, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3153), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3157), 18 },
                    { 15, 18, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3178), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3182), 17 },
                    { 16, 20, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3202), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3207), 12 },
                    { 17, 17, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3227), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3231), 18 },
                    { 18, 16, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3252), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3257), 20 },
                    { 19, 16, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3277), true, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3281), 9 },
                    { 11, 10, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3077), false, new DateTime(2020, 6, 19, 13, 34, 9, 905, DateTimeKind.Local).AddTicks(3082), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Veniam velit nisi qui maiores iste qui et.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(2398), 20, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3066) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Consequatur nesciunt ipsum est qui qui sunt dolores eos quisquam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3629), 14, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3644) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Assumenda qui cumque quaerat aut nihil aut quo aut.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3774), 15, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3780) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quo autem consectetur quis est illo non.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3844), 8, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3848) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Odit dolorem incidunt aspernatur commodi quam aut tempore itaque sed.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3922), 11, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3927) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Ea at quia praesentium qui.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3990), 1, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(3995) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Eius laudantium numquam corporis ducimus vel.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4056), 9, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4060) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Modi ut quam consequatur deleniti debitis accusantium officiis.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4179), 14, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4184) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Quia consequatur ipsum repellat corporis alias temporibus iusto.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4247), 12, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4252) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Ut laboriosam voluptatum rerum commodi sit autem repudiandae eaque ut.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4322), 6, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4327) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Aut aut error et reiciendis.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4377), 12, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4381) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Amet quia minima non nostrum eius alias.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4435), 12, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4440) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Vitae architecto minima enim beatae sit deserunt sint aut.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4503), 3, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4509) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Dolorem maiores id vero accusantium consectetur odio et.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4601), 2, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4606) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Quam beatae sit.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4644), 14, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4649) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Et temporibus totam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4688), 3, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4693) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Adipisci eaque cumque aperiam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4736), 7, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4741) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 11, "Quis quas vel sed et exercitationem.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4791), new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4796) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Consequatur consequatur est ipsam.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4839), 2, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4844) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Consectetur veritatis eos sint dignissimos non nostrum cum sed.", new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4941), 17, new DateTime(2020, 6, 19, 13, 34, 9, 893, DateTimeKind.Local).AddTicks(4946) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(4450), "https://s3.amazonaws.com/uifaces/faces/twitter/gregrwilkinson/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(8463) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9044), "https://s3.amazonaws.com/uifaces/faces/twitter/sementiy/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9066) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9098), "https://s3.amazonaws.com/uifaces/faces/twitter/SULiik/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9102) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9171), "https://s3.amazonaws.com/uifaces/faces/twitter/marcomano_/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9176) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9198), "https://s3.amazonaws.com/uifaces/faces/twitter/dvdwinden/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9202) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9221), "https://s3.amazonaws.com/uifaces/faces/twitter/davidhemphill/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9225) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9244), "https://s3.amazonaws.com/uifaces/faces/twitter/andresenfredrik/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9266), "https://s3.amazonaws.com/uifaces/faces/twitter/benefritz/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9289), "https://s3.amazonaws.com/uifaces/faces/twitter/cbracco/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9293) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9311), "https://s3.amazonaws.com/uifaces/faces/twitter/markretzloff/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9315) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9333), "https://s3.amazonaws.com/uifaces/faces/twitter/cggaurav/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9337) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9354), "https://s3.amazonaws.com/uifaces/faces/twitter/yigitpinarbasi/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9359) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9375), "https://s3.amazonaws.com/uifaces/faces/twitter/safrankov/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9379) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9397), "https://s3.amazonaws.com/uifaces/faces/twitter/lingeswaran/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9402) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9420), "https://s3.amazonaws.com/uifaces/faces/twitter/rtyukmaev/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9424) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9442), "https://s3.amazonaws.com/uifaces/faces/twitter/adhiardana/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9446) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9464), "https://s3.amazonaws.com/uifaces/faces/twitter/paulfarino/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9468) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9486), "https://s3.amazonaws.com/uifaces/faces/twitter/arishi_/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9508), "https://s3.amazonaws.com/uifaces/faces/twitter/overcloacked/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9512) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9529), "https://s3.amazonaws.com/uifaces/faces/twitter/illyzoren/128.jpg", new DateTime(2020, 6, 19, 13, 34, 9, 700, DateTimeKind.Local).AddTicks(9534) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(6847), "https://picsum.photos/640/480/?image=490", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7810), "https://picsum.photos/640/480/?image=645", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7839) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7863), "https://picsum.photos/640/480/?image=251", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7867) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7885), "https://picsum.photos/640/480/?image=128", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7889) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7906), "https://picsum.photos/640/480/?image=854", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7927), "https://picsum.photos/640/480/?image=504", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7931) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7948), "https://picsum.photos/640/480/?image=106", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7952) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7969), "https://picsum.photos/640/480/?image=207", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7973) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7989), "https://picsum.photos/640/480/?image=907", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(7993) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8011), "https://picsum.photos/640/480/?image=524", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8015) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8031), "https://picsum.photos/640/480/?image=59", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8052), "https://picsum.photos/640/480/?image=766", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8056) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8111), "https://picsum.photos/640/480/?image=1011", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8115) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8134), "https://picsum.photos/640/480/?image=436", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8138) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8155), "https://picsum.photos/640/480/?image=165", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8159) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8176), "https://picsum.photos/640/480/?image=526", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8180) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8196), "https://picsum.photos/640/480/?image=186", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8200) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8217), "https://picsum.photos/640/480/?image=756", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8221) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8237), "https://picsum.photos/640/480/?image=1009", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8241) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8258), "https://picsum.photos/640/480/?image=406", new DateTime(2020, 6, 19, 13, 34, 9, 705, DateTimeKind.Local).AddTicks(8262) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8184), true, 5, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8188), 2 },
                    { 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8211), false, 17, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8215), 8 },
                    { 12, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8236), true, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8240), 20 },
                    { 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8262), false, 16, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8267), 14 },
                    { 15, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8314), false, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8318), 18 },
                    { 18, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8392), true, 18, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8396), 11 },
                    { 16, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8340), false, 15, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8344), 6 },
                    { 17, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8366), false, 7, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8370), 13 },
                    { 9, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8157), false, 9, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8161), 2 },
                    { 20, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8508), false, 3, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8512), 4 },
                    { 19, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8418), false, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8422), 9 },
                    { 14, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8289), true, 5, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8293), 7 },
                    { 8, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8130), true, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8134), 4 },
                    { 3, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7993), false, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7998), 17 },
                    { 6, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8076), false, 10, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8081), 21 },
                    { 5, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8048), false, 13, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8053), 5 },
                    { 1, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(6743), false, 7, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7431), 18 },
                    { 4, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8022), true, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8026), 12 },
                    { 2, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7937), true, 11, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(7956), 5 },
                    { 7, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8103), false, 3, new DateTime(2020, 6, 19, 13, 34, 9, 899, DateTimeKind.Local).AddTicks(8108), 18 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "ut", new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(2144), 38, new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(2808) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Vitae quasi nobis cupiditate.
Qui enim architecto quisquam dolores culpa magnam optio.", new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(9751), 24, new DateTime(2020, 6, 19, 13, 34, 9, 886, DateTimeKind.Local).AddTicks(9781) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Ut et excepturi explicabo optio.
Necessitatibus ut rerum fuga aperiam laudantium qui tempore quia.
Rerum omnis quibusdam debitis.
Blanditiis sed qui est accusamus temporibus accusantium.
Similique amet ipsa.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(144), 24, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(152) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Quisquam doloribus autem repudiandae.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(943), 33, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(958) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, @"Quaerat asperiores voluptates.
Ducimus ut veniam.
Voluptas dolor omnis repellendus consequatur consequatur eum maiores.
Laboriosam voluptatem iusto asperiores exercitationem nobis sit ab non.
Iure rerum et dolores rerum rem aut totam.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(1214), 38, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(1220) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Et suscipit voluptas. Natus laboriosam ex dolorum perspiciatis non assumenda eaque. Numquam quae sapiente enim. Accusantium et saepe saepe illum mollitia facilis. Ipsam voluptatem ut a odit repudiandae omnis nostrum non.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2625), 34, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2639) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "assumenda", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2685), 35, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "voluptas", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2922), 40, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2928) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "possimus", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2964), 26, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(2969) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Molestiae rerum dolores aut sint.
Doloremque qui accusantium sunt porro officia qui vero.
Eveniet nobis reprehenderit repellat nemo.
Ipsam placeat eos ab eos quia omnis architecto inventore.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3176), 25, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3182) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Maiores eos sequi sint eos ut aut eaque nobis.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3254), 22, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3258) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Et fugiat vero in.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3310), 34, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3315) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Laborum necessitatibus enim sapiente expedita est.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3371), 30, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3376) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Consectetur perferendis officiis quia quod. Magnam omnis sit est. Nam omnis quibusdam qui molestias quisquam numquam atque. Saepe ratione voluptatem autem magnam aut iste. Et dolores eum quo sed asperiores. Quia maxime impedit.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3603), 23, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3608) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Et porro voluptatibus modi quisquam dolorem molestiae aut.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3668), 36, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3673) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Molestiae quae eligendi dolores officiis.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3726), 38, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3731) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Iure sint laudantium natus qui est omnis aut aspernatur. Magni ratione modi. Qui accusantium nesciunt reprehenderit iste cupiditate id.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3880), 39, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(3885) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Molestiae dolore fugiat. Et facilis laborum dicta voluptas. Omnis neque iusto. Laboriosam placeat laboriosam esse quia. Qui dicta aut pariatur cumque.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4051), 25, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4056) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Eligendi animi asperiores impedit dolor sint.
Nulla ad corrupti provident.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4135), 23, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4140) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Repellat a labore amet doloribus.", new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4193), 33, new DateTime(2020, 6, 19, 13, 34, 9, 887, DateTimeKind.Local).AddTicks(4198) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 19, 13, 34, 9, 740, DateTimeKind.Local).AddTicks(6713), "Jaron_Dickinson@gmail.com", "+CgMD2CHTTpKYcmKqrvrqk90UmaADdkKbI8ZI/KaPQc=", "NH0+z3DKfJOSZuiGcb6ru8jw1me61g4jE/WdT14drRc=", new DateTime(2020, 6, 19, 13, 34, 9, 740, DateTimeKind.Local).AddTicks(7464), "Hailee31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 19, 13, 34, 9, 747, DateTimeKind.Local).AddTicks(7524), "Elyse_Goldner@hotmail.com", "ZKLQeXdC5lJ9Gz1MrdXydn8KcoRdqq8n3C7jxalp0Ok=", "KrK02yJx0wt/LNZI7PUb9sAHU+2E4C67bZYnOxb0yZw=", new DateTime(2020, 6, 19, 13, 34, 9, 747, DateTimeKind.Local).AddTicks(7550), "Stacey_Mosciski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 19, 13, 34, 9, 754, DateTimeKind.Local).AddTicks(6028), "Cole.Schuster79@hotmail.com", "SxJqUgYugOw2vRugQdoQqF5Y31Sshsl3hTI6FfbbyLU=", "HTfbSyXDoYX0oL2Wq8gEhQHiAw7Gw6t+S2SRTYCRtxI=", new DateTime(2020, 6, 19, 13, 34, 9, 754, DateTimeKind.Local).AddTicks(6047), "Cathy59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 13, 34, 9, 761, DateTimeKind.Local).AddTicks(4428), "Yasmine_Mohr49@gmail.com", "oWXHvvsHMYRDwfLu1G3hTINBGN3PU1KJiNKpLBwGQtg=", "Id45DLPHuaq9A2Used3npYSuxTct9vywNeWiYOXXb7E=", new DateTime(2020, 6, 19, 13, 34, 9, 761, DateTimeKind.Local).AddTicks(4442), "Jeramie25" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 13, 34, 9, 769, DateTimeKind.Local).AddTicks(9931), "Laura_OConnell@yahoo.com", "HcLC6m8xQN3bAAgolnSYq7rhNUwy84leP6FgBLdemdg=", "hXTpwws/qxwld+omZ0cUM9TRdF76sf1+ak3T20Rx1+s=", new DateTime(2020, 6, 19, 13, 34, 9, 769, DateTimeKind.Local).AddTicks(9966), "Joanne_Purdy29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 13, 34, 9, 776, DateTimeKind.Local).AddTicks(8333), "Breanne79@hotmail.com", "o0yab/45BgOK+K3HT/ZoBMOLVDicJJpTWnAlsiffRO4=", "+tQG3gvLZh8HJYqriBurqb5Ueh+nHcerhOxKsHoeJl8=", new DateTime(2020, 6, 19, 13, 34, 9, 776, DateTimeKind.Local).AddTicks(8344), "Watson.Donnelly" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 19, 13, 34, 9, 783, DateTimeKind.Local).AddTicks(6609), "Karina_Rice@hotmail.com", "WFGRhd/GNzUVQzsnrA0/5uutb7QfnutFpeFykLsvdeQ=", "YG0xFHOezA8sEd+4SiNOseWYY/gs7jxW+SQx8+w913I=", new DateTime(2020, 6, 19, 13, 34, 9, 783, DateTimeKind.Local).AddTicks(6623), "Sandra.Hessel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 13, 34, 9, 790, DateTimeKind.Local).AddTicks(4843), "Jakayla.Jenkins92@hotmail.com", "aDcMfPdRHrwAASauTRAEHBqXGCSsKB23LytNAgLkdrQ=", "pC6r3Se+keiZEyCERX8WdDrh98mPjfP2Y9COXKqEkSI=", new DateTime(2020, 6, 19, 13, 34, 9, 790, DateTimeKind.Local).AddTicks(4854), "Kiera_Smith6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 19, 13, 34, 9, 797, DateTimeKind.Local).AddTicks(3232), "Madilyn81@hotmail.com", "sDPQ8X/Wr/E87EMQCoDEfNfo7DEuiF/3pexRDwNb6is=", "BV6T6eQqu959ySaw30Ay6rm6hFizRmx8YGA2BGKgJ7Q=", new DateTime(2020, 6, 19, 13, 34, 9, 797, DateTimeKind.Local).AddTicks(3240), "German89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 13, 34, 9, 804, DateTimeKind.Local).AddTicks(1485), "Josh_Volkman66@hotmail.com", "MY4i85zK9gR2ZOfhE0GhBQR1Kqaz3h8zVSPG2w+xY74=", "zF4URcKwPDlB6AD/hScNjnLdLJwEVwrsZzJE79OwcgQ=", new DateTime(2020, 6, 19, 13, 34, 9, 804, DateTimeKind.Local).AddTicks(1494), "Demario.Glover" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 19, 13, 34, 9, 811, DateTimeKind.Local).AddTicks(826), "Cordie94@yahoo.com", "2+1FB23hk/GoebDw5CaC3hkLsgrNFsHl98s6/2EAxaE=", "RvMouP9309O2Ulxx3TDERfV1dYtF16S2FJ9AJ1DYQLA=", new DateTime(2020, 6, 19, 13, 34, 9, 811, DateTimeKind.Local).AddTicks(872), "Anastacio_Windler56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 13, 34, 9, 817, DateTimeKind.Local).AddTicks(9615), "Josephine.Mitchell@yahoo.com", "A+sJ5VBned4Baa7DGGM2x7K1Rxz496NVPtw4b8jTPEY=", "hZD7HBghyyMjcQDQn5B5TYM5mspY0K64+ep8dO8kKFk=", new DateTime(2020, 6, 19, 13, 34, 9, 817, DateTimeKind.Local).AddTicks(9626), "Reese_Hamill" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 19, 13, 34, 9, 824, DateTimeKind.Local).AddTicks(7805), "Heath_Reinger@hotmail.com", "tZF35LIj2QI8+WRCJ4UuYraQ8lL+kzJu8shRg9y5iGg=", "8BUvR961WhF2+QLPX+RqQfu0NEIokkSEELMRN+6GKj8=", new DateTime(2020, 6, 19, 13, 34, 9, 824, DateTimeKind.Local).AddTicks(7816), "Myrtis_Daugherty" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 19, 13, 34, 9, 831, DateTimeKind.Local).AddTicks(6421), "Ansley_Crist@yahoo.com", "7Yp9US8G3JFOU8x7RHe1NzPZbeQC2zi4Fd27LyUc1xo=", "NaZR8IVm+QnB/gaZIHryoUdJ5f3oMuFazF44HgNTA/0=", new DateTime(2020, 6, 19, 13, 34, 9, 831, DateTimeKind.Local).AddTicks(6460), "Mollie_Deckow" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 19, 13, 34, 9, 838, DateTimeKind.Local).AddTicks(5047), "Zula41@gmail.com", "Ki2nt6/ihLfOEoED4e1Mx1MQtSlI0A19T6ySspEQDbE=", "HkddnbKS20EXejjQNFWwJpxpdz3Az61vCk/wFcCAclw=", new DateTime(2020, 6, 19, 13, 34, 9, 838, DateTimeKind.Local).AddTicks(5060), "Sherwood_Turcotte27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 13, 34, 9, 845, DateTimeKind.Local).AddTicks(3276), "Demario.Dach@hotmail.com", "kA9ceWf9OZ8Z6HFYbeJc65FmvcoZFxHUdrdQifMMa9E=", "DOWWl/fe4u2qwnBsBNWJeXm6KE9ryWvE8t1ITzRrNVk=", new DateTime(2020, 6, 19, 13, 34, 9, 845, DateTimeKind.Local).AddTicks(3285), "Lucy_Gusikowski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 19, 13, 34, 9, 852, DateTimeKind.Local).AddTicks(1686), "Johnpaul_Volkman@hotmail.com", "fa27ElZc2bDe9g1SN6phX5eexjagWjp5czwfEUZBHQs=", "lybWrzE1g2hKu6NCqEkkmpi/8IOyoofAKpaggzsiX9E=", new DateTime(2020, 6, 19, 13, 34, 9, 852, DateTimeKind.Local).AddTicks(1714), "Liana92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 19, 13, 34, 9, 859, DateTimeKind.Local).AddTicks(216), "Kaden_Willms6@gmail.com", "P89CM8yeV2rkac+kWUIFWpcy0rJxHgvifUJ56lQmxTc=", "GZEBKsuMwI1Z6jRrHeftipSsUAcRw4CwvQoFTPAJ1Bk=", new DateTime(2020, 6, 19, 13, 34, 9, 859, DateTimeKind.Local).AddTicks(227), "Lizzie.Gerhold" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 19, 13, 34, 9, 865, DateTimeKind.Local).AddTicks(8421), "Jude65@gmail.com", "hF2qK6rHzzf8Sl9qF3ch2k3+lXXHEQJUBPrw0oO5wso=", "VyQLKFB70FiHkbQXP254aV/nwxJF5DfTw3s1+DIkRvs=", new DateTime(2020, 6, 19, 13, 34, 9, 865, DateTimeKind.Local).AddTicks(8431), "Alessia_Dietrich65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 13, 34, 9, 872, DateTimeKind.Local).AddTicks(6665), "Kip.Hermann20@hotmail.com", "Q9VzRLURXuIEi4UCSo2pqDDtr/ftS8E0JO76i+rL5jc=", "gu/MWiMy9mILfZmYWpdAc2BejSjyoaZF6cTYc5UZpLI=", new DateTime(2020, 6, 19, 13, 34, 9, 872, DateTimeKind.Local).AddTicks(6675), "Brooke63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 13, 34, 9, 879, DateTimeKind.Local).AddTicks(4673), "PD6YAXAKOpJ30fROq9TRmizRJG2zFQFMY1fYVD2MsLM=", "/B8OmL4I1rekn1n+fIHJ7THwDZJq9wlIHG/22TGlAqU=", new DateTime(2020, 6, 19, 13, 34, 9, 879, DateTimeKind.Local).AddTicks(4673) });
        }
    }
}
