import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { LikeService } from 'src/app/services/like.service';
import { CommentService } from 'src/app/services/comment.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { Observable, empty, Subject } from 'rxjs';
import { User } from 'src/app/models/user';
import { catchError, takeUntil, switchMap } from 'rxjs/operators';
import { UpdateComment } from 'src/app/models/comment/update-comment';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() removeCommentEvent = new EventEmitter<number>();

    private unsubscribe$ = new Subject<void>();
    public updateCommentData = {} as UpdateComment;
    public loading = false;
    public showEdit = false;
    public showLikes = false;

    public getLikes() { return this.comment.reactions.filter(x => x.isLike === true); }
    public getDislikes() { return this.comment.reactions.filter(x => x.isLike === false).length; }


    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) { }

    public likeComment(state: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, state, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, state, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public UpdateComment() {
        this.updateCommentData.commentId = this.comment.id;
        this.updateCommentData.body = this.comment.body;
        const postSubscription = this.commentService.UpdateComment(this.updateCommentData);

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.comment.body = respPost.body.body;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
        this.showEdit = !this.showEdit;
    }

    public removeComment() {
        const postSubscription = this.commentService.removeComment(this.comment.id);

        this.loading = true;
        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.removeCommentEvent.emit(this.comment.id);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }
    public toggleLikes() {
        this.showLikes = !this.showLikes;
    }

    public toggleEdit() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showEdit = !this.showEdit;
                    }
                });
            return;
        }
        this.showEdit = !this.showEdit;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
