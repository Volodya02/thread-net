import { Component, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { ImgurService } from '../../services/imgur.service';
import { PostService } from '../../services/post.service';
import { User } from '../../models/user';
import { UpdatePost } from '../../models/post/update-post';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    @Output() removeEvent = new EventEmitter<number>();
    public showComments = false;
    public newComment = {} as NewComment;
    public imageFile: File;
    public updatePostData = {} as UpdatePost;
    public loading = false;
    public showEdit = false;

    public showLikes = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private postService: PostService,
        private imgurService: ImgurService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private router: Router
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    //return list of like reactions
    public getLikes() { return this.post.reactions.filter(x => x.isLike === true); }
    //return only number of dislikes
    public getDislikes() { return this.post.reactions.filter(x => x.isLike === false).length; }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public removeComment(id: number) {
        this.post.comments = this.post.comments.filter(x => x.id != id)
    }
    public updatePost() {
        this.updatePostData.postId = this.post.id;
        const postSubscription = !this.imageFile
            ? this.postService.UpdatePost(this.updatePostData)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.updatePostData.previewImage = imageData.body.data.link;
                    return this.postService.UpdatePost(this.updatePostData);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.post.previewImage = respPost.body.previewImage;
                this.post.body = respPost.body.body;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
        this.showEdit = !this.showEdit;
    }

    public SharePost() {
        open(`mailto:?subject=I wanted you to see this http://thread.com &body=${this.post.body} ${this.post.previewImage ? " ; inspiring image link : " + this.post.previewImage : ""} ; postid : ${this.post.id} `);
    }


    public removePost() {
        const postSubscription = this.postService.removePost(this.post.id);

        this.loading = true;
        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.removeEvent.emit(this.post.id);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.updatePostData.previewImage = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.updatePostData.previewImage = undefined;
        this.imageFile = undefined;
    }
    public toggleLikes() {
        this.showLikes = !this.showLikes;
    }
    public toggleEdit() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showEdit = !this.showEdit;
                    }
                });
            return;
        }
        this.updatePostData.body = this.post.body;
        this.updatePostData.previewImage = this.post.previewImage;
        this.showEdit = !this.showEdit;
    }

    public likePost(state: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, state, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, state, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
