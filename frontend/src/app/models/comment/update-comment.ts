export interface UpdateComment {
    commentId: number;
    body: string;
}
