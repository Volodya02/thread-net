export interface UpdatePost {
    postId: number;
    body: string;
    previewImage: string;
}