import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { Comment } from '../models/comment/comment';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }

    public likePost(post: Post, state: boolean, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: state,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        if (!hasReaction) {
            innerPost.reactions = innerPost.reactions.concat({ isLike: state, user: currentUser });
        } else if (hasReaction && innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike === reaction.isLike) {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id != currentUser.id);
        } else {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: state, user: currentUser });
        }
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                if (!hasReaction) {
                    innerPost.reactions = innerPost.reactions.concat({ isLike: state, user: currentUser });
                } else if (hasReaction && innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike === reaction.isLike) {
                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                } else {
                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: state, user: currentUser });
                }

                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, state: boolean, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: state,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);
        if (!hasReaction) {
            innerComment.reactions = innerComment.reactions.concat({ isLike: state, user: currentUser });
        } else if (hasReaction && innerComment.reactions.find((x) => x.user.id === currentUser.id).isLike === reaction.isLike) {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id != currentUser.id);
        } else {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: state, user: currentUser });
        }
        hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                if (!hasReaction) {
                    innerComment.reactions = innerComment.reactions.concat({ isLike: state, user: currentUser });
                } else if (hasReaction && innerComment.reactions.find((x) => x.user.id === currentUser.id).isLike === reaction.isLike) {
                    innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
                } else {
                    innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: state, user: currentUser });
                }

                return of(innerComment);
            })
        );
    }
}
